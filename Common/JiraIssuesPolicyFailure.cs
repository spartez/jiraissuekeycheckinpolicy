﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.TeamFoundation.VersionControl.Client;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

using Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;

namespace Spartez.TFS4JIRA.CheckInPolicy.JiraIssuesPolicy
{
    class JiraIssuesPolicyFailure : PolicyFailure
    {
        public JiraIssuesPolicyFailure(JiraPolicyFailureDetails jiraPolicyFailureDetails, IPolicyEvaluation policy)
            : base(jiraPolicyFailureDetails.Message, policy)
        {
            this.JiraPolicyFailureDetails = jiraPolicyFailureDetails;
        }

        public JiraPolicyFailureDetails JiraPolicyFailureDetails { get; private set; }
    }   
}
