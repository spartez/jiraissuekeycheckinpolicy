﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using System.Text.RegularExpressions;
using System.Windows;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;
using Spartez.TFS4JIRA.CheckInPolicy.Settings;

namespace Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper
{
    public class JiraIssuesPolicyHelper
    {
        private const string ISSUE_REGEXP = "([A-Z][A-Z_0-9]+\\-\\d+)";
        private const string SEARCH_STRING_SEPARATOR = "\n";
        private const string NO_JIRA_URL_MESSAGE = "JIRA URL has not been set. Please set it in the check-in policy configuration. Double click to show configuration window.";
        private const string BAD_CONFIGURATION_MESSAGE = "Error while connection to JIRA. Please check the check-in policy configuration.  Double click to show configuration window.";
        private const string NO_CREDENTIALS_MESSAGE = "You have not configured JIRA connection yet. Double click to show configuration window.";
        private const string FORCE_WINDOW_MESSAGE = "Are you sure you want to remove this policy warning?";
        private const string FORCE_WINDOW_TITLE = "Remove policy failure";

        private static readonly JiraPolicyFailureDetails[] BadConfiguration = { JiraPolicyFailureDetails.GetJiraPolicyFailureDetails(JiraPolicyFailureType.CONFIGURATION_ERROR, BAD_CONFIGURATION_MESSAGE) };
        private static readonly JiraPolicyFailureDetails[] NoJiraUrl = { JiraPolicyFailureDetails.GetJiraPolicyFailureDetails(JiraPolicyFailureType.NO_JIRA_URL, NO_JIRA_URL_MESSAGE) };
        
        private readonly LocalCheckInPolicySettingsStorage _localCheckInPolicySettingsStorage;
        private readonly List<String> _forcedIssueKeys;

        private bool _allowNoIssueKeys;

        public JiraIssuesPolicyHelper()
        {
            _forcedIssueKeys = new List<string>();
            _allowNoIssueKeys = false;
            _localCheckInPolicySettingsStorage = new LocalCheckInPolicySettingsStorage(); ;
        }

        private LocalCheckInPolicySettings GetJiraConnectionSettings(string jiraUrl)
        {
            _localCheckInPolicySettingsStorage.Reload();

            return _localCheckInPolicySettingsStorage.HasSettingsForJiraUrl(jiraUrl)
                    ? _localCheckInPolicySettingsStorage.GetSettingsForJiraUrl(jiraUrl)
                    : new LocalCheckInPolicySettings(jiraUrl);
        }

        private void SaveJiraConnectionSettings(LocalCheckInPolicySettings jiraIssuesPolicySettings)
        {
            _localCheckInPolicySettingsStorage.Reload();
            _localCheckInPolicySettingsStorage.AddSettings(jiraIssuesPolicySettings);
            _localCheckInPolicySettingsStorage.Save();
        }

        public bool Edit(GlobalCheckInPolicySettings globalCheckInPolicySettings, IEnumerable<string> checkInNoteNames)
        {
            LocalCheckInPolicySettings localCheckInPolicySettings = GetJiraConnectionSettings(globalCheckInPolicySettings.JiraUrl);

            SettingsWindow window = new SettingsWindow(globalCheckInPolicySettings, localCheckInPolicySettings, checkInNoteNames, true);
            bool? editSucceed = window.ShowDialog();

            if (editSucceed == true)
            {
                localCheckInPolicySettings.JiraUrl = window.LocalSettings.JiraUrl;
                localCheckInPolicySettings.Login = window.LocalSettings.Login;
                localCheckInPolicySettings.Password = window.LocalSettings.Password;

                SaveJiraConnectionSettings(localCheckInPolicySettings);
                
                return true;
            }

            return false;
        }

        public JiraPolicyFailureDetails[] Evaluate(GlobalCheckInPolicySettings globalCheckInPolicySettings, String comment, IEnumerable<Tuple<String, String>> checkinNotes)
        {
            if (String.IsNullOrWhiteSpace(globalCheckInPolicySettings.JiraUrl))
            {
                return NoJiraUrl;
            }

            if (!_localCheckInPolicySettingsStorage.HasSettingsForJiraUrl(globalCheckInPolicySettings.JiraUrl))
            {
                return new[] { JiraPolicyFailureDetails.GetJiraPolicyFailureDetailsForJira(JiraPolicyFailureType.NO_CREDENTIALS, NO_CREDENTIALS_MESSAGE, globalCheckInPolicySettings.JiraUrl) };
            }

            LocalCheckInPolicySettings localCheckInPolicySettings = GetJiraConnectionSettings(globalCheckInPolicySettings.JiraUrl);
            JiraConnection jiraConnection = new JiraConnection(localCheckInPolicySettings);

            if (jiraConnection.TestConnection() != null)
            {
                return BadConfiguration;
            }

            string searchString = GetStringToSearch(comment, checkinNotes, globalCheckInPolicySettings);

            var issueKeyMatches = Regex.Matches(searchString, ISSUE_REGEXP);
            if (issueKeyMatches.Count == 0 && _allowNoIssueKeys != true)
            {
                string errorMessage = GetNoIssueKeysErrorMessage(globalCheckInPolicySettings);
                return new[] {JiraPolicyFailureDetails.GetJiraPolicyFailureDetails(JiraPolicyFailureType.NO_ISSUE_KEYS, errorMessage)};
            }

            try
            {
                var failures = new List<JiraPolicyFailureDetails>();
                foreach (var key in from Match match in issueKeyMatches select match.Groups[1].Value)
                {
                    if (failures.Any(x => x.IssueKey == key))
                    {
                        continue;
                    }

                    if (_forcedIssueKeys.Contains(key))
                    {
                        continue;
                    }

                    if(!jiraConnection.CheckIfIssueExists(key)) 
                    {
                        failures.Add(JiraPolicyFailureDetails.GetJiraPolicyFailureDetailsForIssue(JiraPolicyFailureType.ISSUE_NOT_FOUND, String.Format("JIRA issue with key '{0}' not found. Double click to remove this warning.", key), key));
                    }
                }
                return failures.ToArray();
            }
            catch (System.ServiceModel.FaultException)
            {
                return BadConfiguration;
            }                    
        }

        private string GetStringToSearch(string checkinComment, IEnumerable<Tuple<string, string>> checkinNotes, GlobalCheckInPolicySettings settings)
        {
            string comment = (settings.ScanCheckInComment) ? checkinComment : "";

            if (!settings.ScanCheckInNote)
            {
                return comment;
            }

            var checkInNote = from note in checkinNotes
                where note.Item1 == settings.ScannedCheckInNoteName
                select note.Item2;

            return checkInNote.Aggregate(comment, (sum, note) => sum + SEARCH_STRING_SEPARATOR + note);
        }

        private string GetNoIssueKeysErrorMessage(GlobalCheckInPolicySettings globalCheckInPolicySettings)
        {
            string errorMessage = "";

            if (globalCheckInPolicySettings.ScanCheckInComment && globalCheckInPolicySettings.ScanCheckInNote)
            {
                errorMessage = String.Format("Please enter at least one JIRA issue key in check-in comment or check-in note '{0}'.",
                    globalCheckInPolicySettings.ScannedCheckInNoteName);
            }
            else if (globalCheckInPolicySettings.ScanCheckInComment)
            {
                errorMessage = "Please enter at least one JIRA issue key in check-in comment.";
            }
            else if (globalCheckInPolicySettings.ScanCheckInNote)
            {
                errorMessage = String.Format("Please enter at least one JIRA issue key in check-in note '{0}'.",
                    globalCheckInPolicySettings.ScannedCheckInNoteName);
            }

            return errorMessage + " Double click to remove this warning.";
        }


        public bool Activate(GlobalCheckInPolicySettings globalCheckInPolicySettings, JiraPolicyFailureDetails failure)
        {
            if (failure.FailureType == JiraPolicyFailureType.NO_ISSUE_KEYS)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show(FORCE_WINDOW_MESSAGE, FORCE_WINDOW_TITLE, MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    _allowNoIssueKeys = true;
                    return true;
                }
            }

            if (failure.FailureType == JiraPolicyFailureType.ISSUE_NOT_FOUND)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show(FORCE_WINDOW_MESSAGE, FORCE_WINDOW_TITLE, MessageBoxButton.YesNo);
                if(messageBoxResult == MessageBoxResult.Yes)
                {
                    _forcedIssueKeys.Add(failure.IssueKey);
                    return true;
                }
            }

            if (failure.FailureType == JiraPolicyFailureType.NO_CREDENTIALS ||
                failure.FailureType == JiraPolicyFailureType.NO_JIRA_URL ||
                failure.FailureType == JiraPolicyFailureType.CONFIGURATION_ERROR)
            {
                LocalCheckInPolicySettings localCheckInPolicySettings = new LocalCheckInPolicySettings(failure.JiraUrl);
                SettingsWindow window = new SettingsWindow(globalCheckInPolicySettings, localCheckInPolicySettings, new String[]{ globalCheckInPolicySettings.ScannedCheckInNoteName }, false);
                bool? editSucceed = window.ShowDialog();

                if (editSucceed == true)
                {
                    localCheckInPolicySettings.Login = window.LocalSettings.Login;
                    localCheckInPolicySettings.Password = window.LocalSettings.Password;

                    SaveJiraConnectionSettings(localCheckInPolicySettings);
                    return true;
                }
            }

            return false;
        }

        public void DisplayHelp(JiraPolicyFailureDetails failure)
        {
            try
            {
                Process.Start(new ProcessStartInfo("explorer.exe", Properties.Resources.WikiHomeUrl));
            }
            catch (Exception)
            {
                MessageBox.Show(failure.Message, "Jira Issue Key Policy Help");
            }
        }
    }
}
