﻿namespace Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper
{
    public class GlobalCheckInPolicySettings
    {
        public string JiraUrl { get; set; }
        public bool ScanCheckInComment { get; set; }
        public bool ScanCheckInNote { get; set; }
        public string ScannedCheckInNoteName { get; set; }
    }
}